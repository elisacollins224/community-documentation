---
title: Outreach Template for Local Situations
keywords: email templates, outreach, local situations
last_updated: July 20, 2018
tags: [helpline_procedures_templates, templates]
summary: "Template for reaching out to clients who might be affected by a local situation"
sidebar: mydoc_sidebar
permalink: 279-Situations_template.html
folder: mydoc
conf: Public
ref: Situations_template
lang: en
---


# Outreach Template for Local Situations
## Template for reaching out to clients who might be affected by a local situation

### Body

Dear xxxxx,

My name is [Name] and I'm an incident handler of Access Now's Digital Security
Helpline. As you requested our assistance in the past, I'm writing to you now
because the Helpline has been alerted to a critical situation that is happening
in your area. [Add description of situation].

We think that due to the ongoing situation, there is a heightened risk of
[identified digital threats]. We would therefore like to suggest you to adopt
the following preventative measures:

- [measure]
- [measure]

I will be glad to assist you in case you need help in implementing our
recommendations, or to resolve any problem you may be already experiencing.

Best,
[Name]


* * *


### Related Articles

- [Article #276: Outreach to Clients for Local Situations](276-Situations.html)
