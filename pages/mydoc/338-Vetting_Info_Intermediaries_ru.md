---
title: Inform Intermediary About Vetting Process - Russian
keywords: email templates, vetting, vetting process, intermediaries
last_updated: February 7, 2019
tags: [helpline_procedures_templates, templates]
summary: "Шаблон для информирования посредника о нашей процедуре проверки"
sidebar: mydoc_sidebar
permalink: 338-Vetting_Info_Intermediaries_ru.html
folder: mydoc
conf: Public
ref: Vetting_Info_Intermediaries
lang: ru
---


# Inform Intermediary About Vetting Process
## Template to inform an intermediary about our vetting process

### Body

Здравствуйте,

Для защиты пользователей, находящихся в уязвимом положении, и для расширения сети доверия мы проверяем потенциальных клиентов нашей службы поддержки.

Мы связываемся с доверенными партнерами Access Now и можем сообщить им имя и электронный адрес клиента ([ИМЯ КЛИЕНТА]). Мы хотим удостовериться, что он(а) в действительности принадлежит к числу гражданских активистов. Вся прочая информация, включая причину обращения в нашу службу поддержки, останется строго конфиденциальной.

Пожалуйста, обратите также внимание на то, что служба поддержки Access Now не предназначена для лиц младше 18 лет. Если вы знаете, что [Client Name] еще нет 18 лет, просим сообщить нам об этом. Мы свяжем [ИМЯ КЛИЕНТА] с партнерами, которые смогут предоставить необходимую поддержку.

Тем временем наша команда продолжит работу над вашим запросом. Если у вас появятся какие-либо вопросы или замечания, пожалуйста, дайте нам знать.

С уважением,

[ИМЯ СОТРУДНИКА]
