---
title: Italian - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "Email da spedire alle persone assistite dalla Helpline alla chiusura di un caso"
sidebar: mydoc_sidebar
permalink: 346-it_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: it
---


# Italian - Feedback Survey Template
## Email da spedire alle persone assistite dalla Helpline alla chiusura di un caso

### Body

Gentile $ClientName,

grazie per aver contattato la Helpline di sicurezza informatica dell'organizzazione internazionale per i diritti umani Access Now - https://accessnow.org.

Con questo messaggio ti informiamo della chiusura del tuo caso, intitolato "{$Ticket-&gt;Subject}".

Per noi i tuoi commenti sono importanti - se vuoi darci un riscontro sulla tua esperienza con la Helpline di sicurezza informatica di Access Now, rispondi alle domande di questo sondaggio:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Il numero del tuo caso è:  {$Ticket-&gt;id}

Se hai altre domande o dubbi, faccelo sapere: siamo felici di aiutarti.

Cordiali saluti,

$IHName
