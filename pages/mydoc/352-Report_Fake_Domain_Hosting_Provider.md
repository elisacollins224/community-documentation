---
title: Template to Report Cloned Website to Hosting Provider
keywords: fake website, impersonation, cloning, phishing, escalations, hosting provider, malicious website
last_updated: March 29, 2019
tags: [fake_domain_templates, templates] 
summary: "Email template to report a cloned or phishing website and request it to be taken down"
sidebar: mydoc_sidebar
permalink: 352-Report_Fake_Domain_Hosting_Provider.html
folder: mydoc
conf: Public
lang: en
---


# Template to Report Cloned Website to Hosting Provider
## Email template to report a cloned or phishing website and request it to be taken down

### Body

TO: abuse@hostingprovider
SUBJECT: Phishing/Cloned Website Report

Dear XXXX,

My name is [IH’s name] and I work at Access Now Digital Security Helpline. We are a free-of-charge resource for civil society around the world. I'm respectfully contacting you to ask for your assistance with a digital security issue that was recently reported to our helpline.

We would like to report a malicious website with this URL: [Fake Website URL]. The website is being used for harmful activities. 

[Explanations and proof of malicious activity. It could be result from threatcrowd.org and urlscan.io]

We are asking for your assistance in taking down this malicious infrastructure, as it might harm more people as long as it is active.

In advance we appreciate your responsiveness, and gently request your immediate action to solve the above-mentioned situation.

Kind regards,

[IH’s name]


* * *


### Related Articles

- [Article #342: Handling of Fake Domains](342-Handling_Fake_Domains.html)
