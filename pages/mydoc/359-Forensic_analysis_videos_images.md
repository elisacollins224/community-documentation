---
title: Forensic Analysis of Videos and Images
keywords: video, images, forensic, malware, SD card
last_updated: August 12, 2019
tags: [forensics, articles]
summary: "Video and images files are sometimes delivered to the Helpline with a request to get information about these files (date of creation, to determine if it could have been manipulated or modified, etc.), so as to use them as digital evidence or identify malware."
sidebar: mydoc_sidebar
permalink: 359-Forensic_analysis_videos_images.html
folder: mydoc
conf: Public
ref: Forensic_analysis_videos_images
lang: en
---

# Forensic Analysis of Videos and Images
## How to handle and analyze a video or image to use it as digital evidence

### Problem

Videos or images have been delivered to the Helpline with a request to get information about these files to use them as digital evidence or identify malware.

Besides the problems highlighted in [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html), we must also consider the Helpline's limitations and scope regarding these cases, and that we might not qualify as expert witnesses despite the findings obtained from the analysis.


* * *


### Solution

#### Preliminary Steps

1. Define the aim of the investigation:

    - **Case 1: Acquire and preserve evidence to present in a court:** please check with the Helpline management to decide how to proceed with this kind of cases. Probably, you will need to reach out to a company specialized in forensics who has experience with the laws of the specific country.

    - **Case 2: Malware analysis or other scenarios:** Clarify the primary goal of the analysis before proceeding with further actions: does the client only want to verify the file information? Do they want to know if the video or images could have been manipulated or modified? Do they suspect that the file is infected? 

2. Read [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html) before proceeding with any action related to this kind of cases. This article should always be followed to avoid any alteration of the evidence collected.

3. Explain to the client the Helpline's approach, limitations, and scope regarding this kind of cases. If the client has the possibility or resources to hire a specialist or contact another organization with a forensic lab, advise them to do so (we can suggest some contacts among CiviCERT partners).

    Please keep in mind (and explain this to the client) that we might not qualify as expert witnesses despite the findings obtained from the analysis.

4. If the client agrees to continue with our service, please explain to them the technical and legal implications: explain the concept of the chain of custody and specific legal requirements/procedures depending on the country's legal code. Also mention that the acquisition and analysis procedures would require additional preparation and technical capacity from both sides (the client's and ours), as the files should be sent in a secure way that ensures their integrity (therefore we should establish a secure communication channel with the client). Some procedures will require some technical knowledge (for example, the byte-copy method) but others are easier to perform (such as getting the hash of the files). So we should assist the client to determine if they should get in-person assistance to perform these actions or are skilled enough to do it by themselves with our remote support.

5. Information about the delivery of the files: ask the client as much information as possible about how they got these files and how they stored them. We need to gain the context about the videos/images acquisition, so please ask the client to provide the following information:

    - Where are the files currently saved?
    - What available operating system and machines are you able to use to handle the files or send them to us?
    - How did you receive these videos (i.e., through email, external storage devices, etc.)?
    - Has any documented compression/decompression operation been performed?
    - Did you verify the integrity of the files upon their reception?
    - Did you generate a master hash of each of the files?

    *If, based on the replies to the questions above, the client shows they're capable of extracting the hashes of the files, we should provide them with instructions on how to do it correctly (see next step).*

6. Generate and compare hashes

    Before proceeding with any action, it is required to document the hash of the first copy of the files received by the client. Therefore, we first need to ask the client to obtain the hash and communicate it to us. When we receive the files, we will then need to generate the hash of the received copy and compare it to the one the client sent. In this way, we can verify the integrity of the files to make sure that what we received is identical to what the client sent.

Guide the client through the steps needed to generate a hash of the files following the instructions in the "Generate the Hash of a File" section of [Article #366: How to Check the Integrity of a File](366-File_Integrity_Check.html).

7. Since we may want to trace back the origin of the files and run some tests, we need a [byte-copy](252-Forensic_Handling_Data.html#how-to-byte-copy-data) of the device (USB stick, external HDD, SD card, etc.) where these documents were delivered to the client in order to perform a full analysis. Metadata analysis can be done just with the document, even if we received it through a cloud service or email, but don't forget to verify the hashes of the files before performing any test.


#### 1. Run the analysis

- **Metadata extraction with ExifTool**
    
    [ExifTool](https://github.com/exiftool/exiftool) is a platform-independent Perl library plus a command-line application for reading, writing and editing meta information in a wide variety of files. Please refer to [this page](http://owl.phy.queensu.ca/~phil/exiftool/) to check the list of file types and meta information formats currently supported by ExifTool and to get the download links. Installation instructions for Windows, Mac, and Unix platforms can be found [here](http://owl.phy.queensu.ca/~phil/exiftool/install.html). Remember to check the [checksums](http://owl.phy.queensu.ca/~phil/exiftool/checksums.txt) to verify these distribution packages.

    *Note: ExifTool does not need to be installed to run. Just download and extract either the full Perl distribution on Mac/Linux (by running ./exiftool in the ExifTool directory), or the Windows EXE version on Windows, and run it directly.*
         
    **Metadata extraction commands:**
        
    - In Windows - *please remember that the stand-alone version ("ExifTool(-k).exe") should be renamed to "exiftool.exe" to allow it to be run by typing "ExifTool" at the command line).*

        Open the command prompt and type the following command:
        
        | **Command format** | exiftool.exe  -t [pathToTheImage]/[ImageFileName].[extension] &gt; [EvidenceTextFile].txt |
        |--------------------|---------------------------------|
        | **Example**        | `exiftool.exe -t Image1.jpg &gt; metadataImage1.txt` |

        Also, you can use the 'drag and drop option': first, change the tool's filename to 'exiftool(-k -a -u -g1 -w txt).exe' (adding inside the brackets this information: `-k -a -u -g1 -w txt`) and then drag-and-drop files and folders to run exiftool on the selected files. This action will generate output ".txt" files with detailed meta information.
  
    - In MacOSX (remember to install the tool in /usr/local/bin) and Linux:
 
        | **Command format** | exiftool [pathToTheImage]/[ImageFileName].[extension] &gt; [EvidenceTextFile].txt |
        |--------------------|---------------------------------|
        | **Example**        | `exiftool  Document/Image1.jpg &gt; metadataImage1.txt` |
    
    
    **Viewing specific metadata properties of a file using tags:**
    
    To view specific metadata properties of a file using tags, just add the specific tag (case is not significant) to the following command:
    
    | **Command format** | exiftool -[tagName] pathToTheImage]/[ImageFileName].[extension] &gt; [EvidenceTextFile].txt |
    |--------------------|---------------------------------|
    | **Example**        | `exiftool -filecreaateddate Document/Image1.jpg &gt; metadataImage1.txt` |
    
    
    Some examples of useful tags:
    
    | Tag name            | Command              | Metadata description                                                                                                                                                                                                                                                                                                                                                  |
    |---------------------|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | FileCreateDate      | -filecreatedate      | the filesystem creation date/time. **Windows/Mac only**. In Windows, the file creation date/time is preserved by default when writing if Win32API::File and Win32::API are available. On Mac, this tag is extracted only if it or the MacOS group is specifically requested or the RequestAll API option is set to 2 or higher. Requires "setfile" for writing on Mac |
    | FileAccessDate      | -fileaccessdate      | The date/time of last access of the file. Note that this access time is updated whenever any software, including ExifTool, reads the file.                                                                                                                                                                                                                            |
    | FileModifyDate      | -filemodifydate      | the filesystem modification date/time. Note that ExifTool may not be able to handle filesystem dates before 1970 depending on the limitations of the system's standard libraries                                                                                                                                                                                      |
    | FileInodeChangeDate | -fileinodechangedate | the date/time when the file's directory information was last changed. **Non-Windows systems only/**                                                                                                                                                                                                                                                                   |
    | FilePermissions     | -filepermission      | r=read, w=write and x=execute permissions for the file owner, group and others. The ValueConv value is an octal number so bit test operations on this value should be done in octal, eg. 'oct($filePermissions#) &amp; 0200'                                                                                                                                              |
    | PreviewImage        | -b -PreviewImage     | Preview image embedded in raw files. It is recommended to use this command on corrupted files.                                                                                                                                                                                                                                                                        |
    | Thumbnails          | -b -ThumbnailImage   | Thumbnail embedded in raw files. It is recommended to use this command on corrupted files.                                                                                                                                                                                                                                                                            |
    

    A complete list of Exiftool tag name can be found [here](http://owl.phy.queensu.ca/~phil/exiftool/TagNames/index.html) and [here](https://sno.phy.queensu.ca/~phil/exiftool/TagNames/Extra.html)
    
    Notes:

    - The -b option tells exiftool to output data in binary format.
    - Tag names sometimes differ from their descriptions. Use the -s command-line option to see the actual tag names instead of the descriptions shown when extracting information.
    - To exclude a tag, add a double dash in front of the command: --[TagnameCommand]. For example, `--gpsposition` will exclude the information on the geographic position in the metadata output.
    - To display the metadata in a specific language, use `-lang [language code]`. For example, add `-lang es` for Spanish or `-lang de` for German.

    Please note that - unlike jpeg files, which have plenty of metadata - mp4 files save few metadata and cannot show information on the dates of creation and modification.

- **Full Analysis using [Autopsy](https://www.sleuthkit.org/autopsy/)**

    Be aware that, in order to perform this procedure, you require the [byte-copy](252-Forensic_Handling_Data.html#how-to-byte-copy-data) of the device where the files were stored.
    
    - Open Autopsy in Kali Linux (this app is built-in in the distro): go to Applications -&gt; Forensic -&gt; Autopsy. A prompt will appear, with a link to open the Autopsy Forensic Browser (`https://localhost:9999/autopsy`).
    - In the Autopsy Forensic Browser, create a new case. From here you can follow the steps detailed [in this guide](https://digital-forensics.sans.org/blog/2009/05/11/a-step-by-step-introduction-to-using-the-autopsy-forensic-browser).
    - An alternative is to use [CAINE](www.caine-live.net) with a VM, which has a nice UI. To create the new case add the image file and select the timeline option.


#### 2. Timeline sequence reconstruction

This is the process of relating video, images, audio, or other data to one another in a chronologically ordered succession. Analysts should be aware that synchronizing multimedia files with different video properties, if not properly accounted for, may result in a drift in synchronization.  The [timeline module](https://www.sleuthkit.org/autopsy/timeline.php) in Autopsy might be especially useful in this analysis. The timeline will only be able to show when a file was last modified, accessed or changed - what is known as the [the MAC times](https://www.forensicswiki.org/wiki/MAC_times), which are not able to keep a complete history of when the files were accessed or changed.

*Note:* experts recommend to create a timeline using a different tool to double-check the results (to find more sources proofing the timestamp), for example:
    
- *Mactime:* Sleuthkit includes the mactime tool, which does the same as Autopsy but requires some additional steps. In this [guide](https://www.cs.nmt.edu/~df/Labs/Lab06_sol.pdf) you will find a detailed explanation on how to use this command.
- *Filesystem:* this depends on the operating system of the device where the files were created/saved/copied/delivered. Please refer to [this paper](https://www.sans.org/reading-room/whitepapers/forensics/filesystem-timestamps-tick-36842) to learn more about this topic.
- *Exiftool:* See the section above on how to obtain metadata from the file, checking the tags `FileModifyDate`, `FileAccessDate`, and `FileInodeChangeDate`.
- *[Windows Registry Entries/information](https://support.microsoft.com/en-us/help/256986/windows-registry-information-for-advanced-users)*  with [RegRipper](https://tools.kali.org/forensics/regripper), a Windows registry forensics tool.
       
If all the 4 timestamps match, the level of proof increases. But there is one downside with timestamps you should be aware of: they only indicate when a file was "last" created, accessed or modified. So if a document was modified twice, you can often see just the last modification time stamp.
    
##### Possible Scenario: files were delivered to the client using a USB stick
    
In this case, we require a [byte-copy](252-Forensic_Handling_Data.html#how-to-byte-copy-data) of the USB stick in order to run the tests in Autopsy (and in the other tools) to get the timeline.

On the USB drive, you should see that the "creation" timestamp is newer than the "content last changed" timestamp. This is an indicator that the files were created on another media and then copied to the USB stick. Here the "content last changed" timestamp may give hints on the creation or last modification of the file. If the creation, content last changed, and metadata last changed timestamps are the same on the original media (please don't forget to do a byte-copy), then it is very likely that the data was not further modified. In this case, when checking the [Registry Entries](https://docs.microsoft.com/en-us/cpp/atl/registry-entries?view=vs-2019) (document opened) with [RegRipper](https://tools.kali.org/forensics/regripper), it is recommended to use the plugins related to USB drives to get the activity of the inserted devices and especially what the key enumerates **SYSTEM \ ControlSet001 \ Enum \ USB \\** and others (this is the path to the place where the USB information can be found, according to the [structure of the registry](https://docs.microsoft.com/en-us/windows/win32/sysinfo/structure-of-the-registry) that exist from the USB drive in the Windows' registry. We should especially check the dates of insertion of the devices, and correlate them with the Autopsy timeline.

- More information on USB History Viewing can be found [here](https://www.forensicswiki.org/wiki/USB_History_Viewing).
- It is also recommended to get the full USB Forensic information by using a tool such as [FTK Imager](https://accessdata.com/product-download/ftk-imager-version-3.4.3). A detail tutorial can be found [here](USB Forensics – Reconstruction of Digital Evidence from USB Drive).
    
#### 3. Authenticity Analysis

Regarding the authenticity analysis of a video, these steps are recommended:

- Analysis of metadata to determine if you can find some unusual data like codecs specific to a software edition in the properties of the file (use Exiftool with the tag `ProcessingSoftware` to check).
- Analysis of sequence and continuity: This corresponds to an exhaustive visual check, frame by frame, in order to capture some anomaly and/or the presence of transitions, as well as the synchronization of sounds with images.
- Frame analysis: After having identified the possible transitions in the step above, you will need to analyze the frame independently to rule out if the transitions are due to a pause in the recording process, a failed recording technique, or editing.
- Finally, if the video contains audio, perform an analysis of spectral sound to see if there is any unusual variation in the background sound, given that in most videos with audio the sound landscape remains constant, and if you find a fragment without the sound it could be an editing point.

#### 4. Date/hour alteration

It should be noted that there are some techniques to change timestamps in files (such as modify the computer’s BIOS clock, privacy cleaners, timestomping, etc.). However, most of these techniques leave behind  some artifacts that examiners can analyze to track these date changes. Read more about how to spot this [here](https://www.sans.org/reading-room/whitepapers/forensics/filesystem-timestamps-tick-36842) and [here](https://www.sba-research.org/wp-content/uploads/publications/hiccups.pdf).

#### 5. Report

Compile all the findings in a report and add this information to an encrypted external medium or container created for the case. Remember that in this data acquisition process, sensitive data on the client may be found in the byte-copy image and should be handled carefully.


* * *


### Comments
#### References

- [Overview Of Computer Forensics Linux Distributions](https://resources.infosecinstitute.com/category/computerforensics/introduction/free-open-source-tools/overview-of-computer-forensics-linux-distributions/#gref)
- [SWGDE Best Practices for Digital Forensic Video Analysis](https://www.swgde.org/documents/Current%20Documents/SWGDE%20Best%20Practices%20for%20Digital%20Forensic%20Video%20Analysis)
- [How to tell if a video recording has been edited](http://www.videoforensicexpert.com/how-to-tell-if-a-video-recording-has-been-edited/)
- [FAQ - How do you detect tampering and alterations?](http://forensicprotection.com/faq_Authentication.html)
- [Multimedia Authentication Testing](http://forensicworkinggroup.com/MAT.pdf)
- [Filesystem Timestamps SANS paper](https://www.sans.org/reading-room/whitepapers/forensics/filesystem-timestamps-tick-36842)
- [Timestamp hiccups: Detecting manipulated filesystem timestamps on NTFS](https://www.sba-research.org/wp-content/uploads/publications/hiccups.pdf)
- [Windows Registry DFIR training](https://www.dfir.training/resources/downloads/windows-registry)
- [Tutorial ReRipper in Linux](https://resources.infosecinstitute.com/registry-forensics-regripper-command-line-linux/)
- [Mactime output](https://wiki.sleuthkit.org/index.php?title=Mactime_output)
- [Timeline Mode](http://www.sleuthkit.org/autopsy/help/tl.html)


* * *

### Related Articles

- [Article #252: Forensic Handling of Data](252-Forensic_Handling_Data.html)
- [Article #366: How to Check the Integrity of a File](366-File_Integrity_Check.html)
